from django.db import models

class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.name}"


class Step(models.Model):
    recipe = models.ForeignKey(
    "Recipe", related_name ="steps", on_delete=models.CASCADE)
    order = models.SmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField(
        "FoodItem", null=True, blank=True)


    def __str__(self):
        return f"{self.recipe}"

class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return f"{self.name}"

class FoodItem(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"

class Ingredient(models.Model):
    recipe = models.ForeignKey(
    "Recipe", related_name ="ingredient", on_delete=models.CASCADE)
    amount = models.SmallIntegerField(null=True)
    measure = models.ForeignKey("Measure", on_delete=models.PROTECT)
    food = models.ForeignKey("FoodItem", on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.food}"
